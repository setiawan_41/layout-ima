import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../ui/views/home/home.component';

export const  MainRoutingComponent = [
    HomeComponent
]

const routes: Routes = [
    {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
    },
    {
        path: 'home',
        component: HomeComponent,
        children: [
            {
                path: '',
                loadChildren: '../module/home/home.module#HomeModule'
            }
        ]
    }
]  

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class MainRouting { }