import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from 'src/app/ui/views/home/home.component';
import { MainComponent } from 'src/app/ui/views/home/main/main.component';

const routes: Routes = [
  {
      path:'',
    //   component: HomeComponent,
      redirectTo: 'main',
      pathMatch: 'full'
  }, 
//   {
//     path: 'main',
//     component: MainComponent,
//     data: { title: 'home' }
//   }
]

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
})

export class HomeRouting { }

export const HomeRoutingComponent = [
    
]