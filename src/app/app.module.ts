import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LayoutDefaultComponent } from './ui/main/layout-default/layout-default.component';
import { ErrorNotfoundComponent } from './ui/main/error-notfound/error-notfound.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { LandingPageComponent } from './ui/main/layout-default/landing-page/landing-page.component';
import { HomeComponent } from './ui/views/home/home.component';
import { DiscoverComponent } from './ui/views/discover/discover.component';
import { MainComponent } from './ui/views/home/main/main.component';
@NgModule({
  declarations: [
    AppComponent,
    LayoutDefaultComponent,
    // ErrorNotfoundComponent,
    LandingPageComponent,
    // MainComponent,
    // HomeComponent,
    // DiscoverComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
