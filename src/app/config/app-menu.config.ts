export const APP_MENU = [
    {
      segment: 'home',
      name: 'Home',
      title: 'Home',
      link: '/home',
      icon: 'icon-home2',
      main_page: false,
      display: true,
      redirect_to: '/home/overview',
      children: [
        {
          segment: 'overview',
          name: 'Overview',
          title: 'Overview',
          link: '/home/overview',
          icon: 'home icon',
          main_page: true,
          display: true,
          redirect_to: '/home/overview',
          children: []
        },
      ]
    },
    {
      segment: 'discover',
      name: 'Discover',
      title: 'Discover',
      link: '/discover',
      icon: 'icon-safari',
      main_page: false,
      display: true,
      redirect_to: '/discover',
      children: [
        {
          segment: 'content',
          name: 'Content',
          title: 'Content',
          link: '/discover/content',
          icon: 'file alternate outline icon',
          main_page: false,
          display: true,
          redirect_to: '/discover/content/index',
          children: [
            {
              segment: 'index',
              name: 'Content',
              title: 'Content',
              link: '/discover/content/index',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/discover/content/index',
              children: []
            },
            {
              segment: 'online-news',
              name: 'News Content',
              title: 'News Content',
              link: '/discover/content/online-news',
              icon: 'icon-newspaper',
              main_page: false,
              display: true,
              redirect_to: '/discover/content/online-news',
              children: []
            },
            {
              segment: 'statement',
              name: 'Statement Content',
              title: 'Statement Content',
              link: '/discover/content/statement',
              icon: 'icon-bubble-lines4',
              main_page: false,
              display: true,
              redirect_to: '/discover/content/statement',
              children: []
            },
            {
              segment: 'picture',
              name: 'Picture Content',
              title: 'Picture Content',
              link: '/discover/content/picture',
              icon: 'icon-stack-picture',
              main_page: false,
              display: true,
              redirect_to: '/discover/content/picture',
              children: []
            },
            {
              segment: 'twitter',
              name: 'Twitter Content',
              title: 'Twitter Content',
              link: '/discover/content/twitter',
              icon: 'icon-twitter',
              main_page: false,
              display: true,
              redirect_to: '/discover/content/twitter',
              children: []
            },
            {
              segment: 'facebook',
              name: 'Facebook Content',
              title: 'Facebook Content',
              link: '/discover/content/facebook',
              icon: 'icon-facebook',
              main_page: false,
              display: true,
              redirect_to: '/discover/content/facebook',
              children: []
            },
          ]
        },
        {
          segment: 'person',
          name: 'Person',
          title: 'Person',
          link: '/discover/person',
          icon: 'user icon',
          main_page: false,
          display: true,
          redirect_to: '/discover/person/index',
          children: [
            {
              segment: 'index',
              name: 'Person',
              title: 'Person',
              link: '/discover/person/index',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/discover/person/index',
              children: []
            },
            {
              segment: 'exposure',
              name: 'Person Exposure',
              title: 'Person Exposure',
              link: '/discover/person/exposure',
              icon: 'icon-statistics',
              main_page: false,
              display: true,
              redirect_to: '/discover/person/exposure',
              children: []
            },
            {
              segment: 'timeline',
              name: 'Person Timeline',
              title: 'Person Timeline',
              link: '/discover/person/timeline',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/discover/person/timeline',
              children: []
            },
            {
              segment: 'trending',
              name: 'Person Trending',
              title: 'Person Trending',
              link: '/discover/person/trending',
              icon: 'icon-fire',
              main_page: false,
              display: true,
              redirect_to: '/discover/person/trending',
              children: []
            },
          ]
        },
        {
          segment: 'organization',
          name: 'Organization',
          title: 'Organization',
          link: '/discover/organization',
          icon: 'building outline icon',
          main_page: false,
          display: true,
          redirect_to: '/discover/organization/index',
          children: [
            {
              segment: 'index',
              name: 'Organization',
              title: 'Organization',
              link: '/discover/organization/index',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/discover/organization/index',
              children: []
            },
            {
              segment: 'exposure',
              name: 'Organization Exposure',
              title: 'Organization Exposure',
              link: '/discover/organization/exposure',
              icon: 'icon-statistics',
              main_page: false,
              display: true,
              redirect_to: '/discover/organization/exposure',
              children: []
            },
            {
              segment: 'timeline',
              name: 'Organization Timeline',
              title: 'Organization Timeline',
              link: '/discover/organization/timeline',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/discover/organization/timeline',
              children: []
            },
            {
              segment: 'trending',
              name: 'Organization Trending',
              title: 'Organization Trending',
              link: '/discover/organization/trending',
              icon: 'icon-fire',
              main_page: false,
              display: true,
              redirect_to: '/discover/organization/trending',
              children: []
            },
          ]
        },
        {
          segment: 'location',
          name: 'Location',
          title: 'Location',
          link: '/discover/location',
          icon: 'compass outline icon',
          main_page: false,
          display: true,
          redirect_to: '/discover/location/index',
          children: [
            {
              segment: 'index',
              name: 'Location',
              title: 'Location',
              link: '/discover/location/index',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/discover/location/index',
              children: []
            },
            {
              segment: 'exposure',
              name: 'Location Exposure',
              title: 'Location Exposure',
              link: '/discover/location/exposure',
              icon: 'icon-statistics',
              main_page: false,
              display: true,
              redirect_to: '/discover/location/exposure',
              children: []
            },
            {
              segment: 'timeline',
              name: 'Location Timeline',
              title: 'Location Timeline',
              link: '/discover/location/timeline',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/discover/location/timeline',
              children: []
            },
            {
              segment: 'trending',
              name: 'Location Trending',
              title: 'Location Trending',
              link: '/discover/location/trending',
              icon: 'icon-fire',
              main_page: false,
              display: true,
              redirect_to: '/discover/location/trending',
              children: []
            },
          ]
        },
        {
          segment: 'online-news',
          name: 'Online News',
          title: 'Online News',
          link: '/discover/online-news',
          icon: 'newspaper outline icon',
          main_page: false,
          display: true,
          redirect_to: '/discover/online-news/index',
          children: [
            {
              segment: 'index',
              name: 'Online News',
              title: 'Online News',
              link: '/discover/online-news/index',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/discover/online-news/index',
              children: []
            },
            {
              segment: 'exposure',
              name: 'Online News Exposure',
              title: 'Online News Exposure',
              link: '/discover/online-news/exposure',
              icon: 'icon-statistics',
              main_page: false,
              display: true,
              redirect_to: '/discover/online-news/exposure',
              children: []
            },
            {
              segment: 'timeline',
              name: 'Online News Timeline',
              title: 'Online News Timeline',
              link: '/discover/online-news/timeline',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/discover/online-news/timeline',
              children: []
            },
            {
              segment: 'trending',
              name: 'Online News Trending',
              title: 'Online News Trending',
              link: '/discover/online-news/trending',
              icon: 'icon-fire',
              main_page: false,
              display: true,
              redirect_to: '/discover/online-news/trending',
              children: []
            },
          ]
        },
        {
          segment: 'printed-news',
          name: 'Printed News',
          title: 'Printed News',
          link: '/discover/printed-news',
          icon: 'print icon',
          main_page: false,
          display: true,
          redirect_to: '/discover/printed-news/index',
          children: [
            {
              segment: 'index',
              name: 'Printed News',
              title: 'Printed News',
              link: '/discover/printed-news/index',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/discover/printed-news/index',
              children: []
            },
            {
              segment: 'exposure',
              name: 'Printed News Exposure',
              title: 'Printed News Exposure',
              link: '/discover/printed-news/exposure',
              icon: 'icon-statistics',
              main_page: false,
              display: true,
              redirect_to: '/discover/printed-news/exposure',
              children: []
            },
            {
              segment: 'timeline',
              name: 'Printed News Timeline',
              title: 'Printed News Timeline',
              link: '/discover/printed-news/timeline',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/discover/printed-news/timeline',
              children: []
            },
            {
              segment: 'trending',
              name: 'Printed News Trending',
              title: 'Printed News Trending',
              link: '/discover/printed-news/trending',
              icon: 'icon-fire',
              main_page: false,
              display: true,
              redirect_to: '/discover/printed-news/trending',
              children: []
            },
          ]
        },
        {
          segment: 'tv-news',
          name: 'TV News',
          title: 'TV News',
          link: '/discover/tv-news',
          icon: 'tv icon',
          main_page: false,
          display: true,
          redirect_to: '/discover/tv-news/index',
          children: [
            {
              segment: 'index',
              name: 'TV News',
              title: 'TV News',
              link: '/discover/tv-news/index',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/discover/tv-news/index',
              children: []
            },
            {
              segment: 'exposure',
              name: 'TV News Exposure',
              title: 'TV News Exposure',
              link: '/discover/tv-news/exposure',
              icon: 'icon-statistics',
              main_page: false,
              display: true,
              redirect_to: '/discover/tv-news/exposure',
              children: []
            },
            {
              segment: 'timeline',
              name: 'TV News Timeline',
              title: 'TV News Timeline',
              link: '/discover/tv-news/timeline',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/discover/tv-news/timeline',
              children: []
            },
            {
              segment: 'trending',
              name: 'TV News Trending',
              title: 'TV News Trending',
              link: '/discover/tv-news/trending',
              icon: 'icon-fire',
              main_page: false,
              display: true,
              redirect_to: '/discover/tv-news/trending',
              children: []
            },
          ]
        },
        {
          segment: 'twitter',
          name: 'Twitter',
          title: 'Twitter',
          link: '/discover/twitter',
          icon: 'twitter icon',
          main_page: false,
          display: true,
          redirect_to: '/discover/twitter/index',
          children: [
            {
              segment: 'index',
              name: 'Twitter',
              title: 'Twitter',
              link: '/discover/twitter/index',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/discover/twitter/index',
              children: []
            },
            {
              segment: 'dashboard',
              name: 'Twitter Dashboard',
              title: 'Twitter Dashboard',
              link: '/discover/twitter/dashboard',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/discover/twitter/dashboard',
              children: []
            },
            {
              segment: 'timeline',
              name: 'Twitter Timeline',
              title: 'Twitter Timeline',
              link: '/discover/twitter/timeline',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/discover/twitter/timeline',
              children: []
            },
            {
              segment: 'statistic',
              name: 'Twitter Statistic',
              title: 'Twitter Statistic',
              link: '/discover/twitter/statistic',
              icon: 'icon-stats-bars',
              main_page: false,
              display: true,
              redirect_to: '/discover/twitter/statistic',
              children: []
            },
            {
              segment: 'trending',
              name: 'Twitter Trending',
              title: 'Twitter Trending',
              link: '/discover/twitter/trending',
              icon: 'icon-fire',
              main_page: false,
              display: true,
              redirect_to: '/discover/twitter/trending',
              children: []
            },
            {
              segment: 'sentiment',
              name: 'Twitter Sentiment',
              title: 'Twitter Sentiment',
              link: '/discover/twitter/sentiment',
              icon: 'icon-heart6',
              main_page: false,
              display: true,
              redirect_to: '/discover/twitter/sentiment',
              children: []
            },
          ]
        },
        {
          segment: 'facebook',
          name: 'Facebook',
          title: 'Facebook',
          link: '/discover/facebook',
          icon: 'facebook f icon',
          main_page: false,
          display: true,
          redirect_to: '/discover/facebook/index',
          children: [
            {
              segment: 'index',
              name: 'Facebook',
              title: 'Facebook',
              link: '/discover/facebook/index',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/discover/facebook/index',
              children: []
            },
            {
              segment: 'dashboard',
              name: 'Facebook Dashboard',
              title: 'Facebook Dashboard',
              link: '/discover/facebook/dashboard',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/discover/facebook/dashboard',
              children: []
            },
            {
              segment: 'timeline',
              name: 'Facebook Timeline',
              title: 'Facebook Timeline',
              link: '/discover/facebook/timeline',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/discover/facebook/timeline',
              children: []
            },
            {
              segment: 'statistic',
              name: 'Facebook Statistic',
              title: 'Facebook Statistic',
              link: '/discover/facebook/statistic',
              icon: 'icon-stats-bars',
              main_page: false,
              display: true,
              redirect_to: '/discover/facebook/statistic',
              children: []
            },
            {
              segment: 'trending',
              name: 'Facebook Trending',
              title: 'Facebook Trending',
              link: '/discover/facebook/trending',
              icon: 'icon-fire',
              main_page: false,
              display: true,
              redirect_to: '/discover/facebook/trending',
              children: []
            },
            {
              segment: 'sentiment',
              name: 'Facebook Sentiment',
              title: 'Facebook Sentiment',
              link: '/discover/facebook/sentiment',
              icon: 'icon-heart6',
              main_page: false,
              display: true,
              redirect_to: '/discover/facebook/sentiment',
              children: []
            },
          ]
        },
        {
          segment: 'trending-issue',
          name: 'Trending Issue',
          title: 'Trending Issue',
          link: '/discover/trending-issue',
          icon: 'star icon',
          main_page: false,
          display: true,
          redirect_to: '/discover/trending-issue',
          children: []
        },
        {
          segment: 'explorer',
          name: 'Explorer Discover',
          title: 'Explorer Discover',
          link: '/discover/explorer',
          icon: 'book icon',
          main_page: false,
          display: true,
          redirect_to: '/discover/explorer',
          children: []
        },
        {
          segment: 'multiple-network',
          name: 'Multiple Network',
          title: 'Multiple Network',
          link: '/discover/multiple-network',
          icon: 'joomla icon',
          main_page: false,
          display: true,
          redirect_to: '/discover/multiple-network',
          children: []
        },
      ]
    },
    {
      segment: 'analyze',
      name: 'Analyze',
      title: 'Analyze',
      link: '/analyze',
      icon: 'icon-pulse2',
      main_page: false,
      display: true,
      redirect_to: '/analyze',
      children: [
        {
          segment: 'select-criteria',
          name: 'Select Criteria',
          title: 'Select Criteria',
          link: '/select-criteria',
          icon: 'check icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/select-criteria',
          children: []
        },
        {
          segment: 'dashboard',
          name: 'Dashboard',
          title: 'Dashboard',
          link: '/analyze/dashboard',
          icon: 'cube icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/dashboard/online-news/content',
          children: [
            {
              segment: 'online-news',
              name: 'Dashboard',
              title: 'Dashboard',
              link: '/analyze/dashboard/online-news',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/analyze/dashboard/online-news/content',
              children: [
                {
                  segment: 'content',
                  name: 'News Content Dashboard',
                  title: 'News Content Dashboard',
                  link: '/analyze/dashboard/online-news/content',
                  icon: 'icon-files-empty',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'statistic',
                  name: 'News Statistic Dashboard',
                  title: 'News Statistic Dashboard',
                  link: '/analyze/dashboard/online-news/statistic',
                  icon: 'icon-stats-bars',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'twitter',
              name: 'Dashboard',
              title: 'Dashboard',
              link: '/analyze/dashboard/twitter',
              icon: 'icon-twitter',
              main_page: false,
              display: true,
              redirect_to: '/analyze/dashboard/twitter/content',
              children: [
                {
                  segment: 'content',
                  name: 'Twitter Content Dashboard',
                  title: 'Twitter Content Dashboard',
                  link: '/analyze/dashboard/twitter/content',
                  icon: 'icon-files-empty',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'statistic',
                  name: 'Twitter Statistic Dashboard',
                  title: 'Twitter Statistic Dashboard',
                  link: '/analyze/dashboard/twitter/statistic',
                  icon: 'icon-stats-bars',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'facebook',
              name: 'Dashboard',
              title: 'Dashboard',
              link: '/analyze/dashboard/facebook',
              icon: 'icon-facebook',
              main_page: false,
              display: true,
              redirect_to: '/analyze/dashboard/facebook/content',
              children: [
                {
                  segment: 'content',
                  name: 'Facebook Content Dashboard',
                  title: 'Facebook Content Dashboard',
                  link: '/analyze/dashboard/facebook/content',
                  icon: 'icon-files-empty',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'statistic',
                  name: 'Facebook Statistic Dashboard',
                  title: 'Facebook Statistic Dashboard',
                  link: '/analyze/dashboard/facebook/statistic',
                  icon: 'icon-stats-bars',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'printed-news',
              name: 'Dashboard',
              title: 'Dashboard',
              link: '/analyze/dashboard/printed-news',
              icon: 'icon-printer4',
              main_page: false,
              display: true,
              redirect_to: '/analyze/dashboard/printed-news/content',
              children: [
                {
                  segment: 'content',
                  name: 'Printed News Content Dashboard',
                  title: 'Printed News Content Dashboard',
                  link: '/analyze/dashboard/printed-news/content',
                  icon: 'icon-files-empty',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'statistic',
                  name: 'Printed News Statistic Dashboard',
                  title: 'Printed News Statistic Dashboard',
                  link: '/analyze/dashboard/printed-news/statistic',
                  icon: 'icon-stats-bars',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'tv-news',
              name: 'Dashboard',
              title: 'Dashboard',
              link: '/analyze/dashboard/tv-news',
              icon: 'icon-tv',
              main_page: false,
              display: true,
              redirect_to: '/analyze/dashboard/tv-news/content',
              children: [
                {
                  segment: 'content',
                  name: 'TV News Content Dashboard',
                  title: 'TV News Content Dashboard',
                  link: '/analyze/dashboard/tv-news/content',
                  icon: 'icon-files-empty',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'statistic',
                  name: 'TV News Statistic Dashboard',
                  title: 'TV News Statistic Dashboard',
                  link: '/analyze/dashboard/tv-news/statistic',
                  icon: 'icon-stats-bars',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
          ]
        },
        {
          segment: 'timeline',
          name: 'Timeline',
          title: 'Timeline',
          link: '/analyze/timeline',
          icon: 'list alternate outline icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/timeline/online-news',
          children: [
            {
              segment: 'online-news',
              name: 'News Timeline',
              title: 'News Timeline',
              link: '/analyze/timeline/online-news',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/analyze/timeline/online-news',
              children: []
            },
            {
              segment: 'twitter',
              name: 'Twitter Timeline',
              title: 'Twitter Timeline',
              link: '/analyze/timeline/twitter',
              icon: 'icon-twitter',
              main_page: false,
              display: true,
              redirect_to: '/analyze/timeline/twitter',
              children: []
            },
            {
              segment: 'facebook',
              name: 'Facebook Timeline',
              title: 'Facebook Timeline',
              link: '/analyze/timeline/facebook',
              icon: 'icon-facebook',
              main_page: false,
              display: true,
              redirect_to: '/analyze/timeline/facebook',
              children: []
            },
            {
              segment: 'printed-news',
              name: 'Printed News Timeline',
              title: 'Printed News Timeline',
              link: '/analyze/timeline/printed-news',
              icon: 'icon-printer4',
              main_page: false,
              display: true,
              redirect_to: '/analyze/timeline/printed-news',
              children: []
            },
            {
              segment: 'tv-news',
              name: 'TV News Timeline',
              title: 'TV News Timeline',
              link: '/analyze/timeline/tv-news',
              icon: 'icon-display',
              main_page: false,
              display: true,
              redirect_to: '/analyze/timeline/tv-news',
              children: []
            },
          ]
        },
        {
          segment: 'top-issue',
          name: 'Top Issue',
          title: 'Top Issue',
          link: '/analyze/top-issue',
          icon: 'hotjar icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/top-issue/online-news/actual',
          children: [
            {
              segment: 'online-news',
              name: 'Issue',
              title: 'Issue',
              link: '/analyze/top-issue/online-news',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/analyze/top-issue/online-news/actual',
              children: [
                {
                  segment: 'actual',
                  name: 'News Actual Issue',
                  title: 'News Actual Issue',
                  link: '/analyze/top-issue/online-news/actual',
                  icon: 'icon-feed',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'history',
                  name: 'News History Issue',
                  title: 'News History Issue',
                  link: '/analyze/top-issue/online-news/history',
                  icon: 'icon-loop',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'comparison',
                  name: 'News Comparison Issue',
                  title: 'News Comparison Issue',
                  link: '/analyze/top-issue/online-news/comparison',
                  icon: 'icon-image-compare',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'twitter',
              name: 'Issue',
              title: 'Issue',
              link: '/analyze/top-issue/twitter',
              icon: 'icon-twitter',
              main_page: false,
              display: true,
              redirect_to: '/analyze/top-issue/twitter/actual',
              children: [
                {
                  segment: 'actual',
                  name: 'Twitter Actual Issue',
                  title: 'Twitter Actual Issue',
                  link: '/analyze/top-issue/twitter/actual',
                  icon: 'icon-feed',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'comparison',
                  name: 'Twitter Comparison Issue',
                  title: 'Twitter Comparison Issue',
                  link: '/analyze/top-issue/twitter/comparison',
                  icon: 'icon-image-compare',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'facebook',
              name: 'Issue',
              title: 'Issue',
              link: '/analyze/top-issue/facebook',
              icon: 'icon-facebook',
              main_page: false,
              display: true,
              redirect_to: '/analyze/top-issue/facebook/actual',
              children: [
                {
                  segment: 'actual',
                  name: 'Facebook Actual Issue',
                  title: 'Facebook Actual Issue',
                  link: '/analyze/top-issue/facebook/actual',
                  icon: 'icon-feed',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'comparison',
                  name: 'Facebook Comparison Issue',
                  title: 'Facebook Comparison Issue',
                  link: '/analyze/top-issue/facebook/comparison',
                  icon: 'icon-image-compare',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'printed-news',
              name: 'Issue',
              title: 'Issue',
              link: '/analyze/top-issue/printed-news',
              icon: 'icon-printer4',
              main_page: false,
              display: true,
              redirect_to: '/analyze/top-issue/printed-news/actual',
              children: [
                {
                  segment: 'actual',
                  name: 'Printed News Actual Issue',
                  title: 'Printed News Actual Issue',
                  link: '/analyze/top-issue/printed-news/actual',
                  icon: 'icon-feed',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'history',
                  name: 'Printed News History Issue',
                  title: 'Printed News History Issue',
                  link: '/analyze/top-issue/printed-news/history',
                  icon: 'icon-loop',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'comparison',
                  name: 'Printed News Comparison Issue',
                  title: 'Printed News Comparison Issue',
                  link: '/analyze/top-issue/printed-news/comparison',
                  icon: 'icon-image-compare',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'tv-news',
              name: 'Issue',
              title: 'Issue',
              link: '/analyze/top-issue/tv-news',
              icon: 'icon-tv',
              main_page: false,
              display: true,
              redirect_to: '/analyze/top-issue/tv-news/actual',
              children: [
                {
                  segment: 'actual',
                  name: 'TV News Actual Issue',
                  title: 'TV News Actual Issue',
                  link: '/analyze/top-issue/tv-news/actual',
                  icon: 'icon-feed',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
          ]
        },
        {
          segment: 'top-person',
          name: 'Top Person',
          title: 'Top Person',
          link: '/analyze/top-person',
          icon: 'users icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/top-person/online-news/index',
          children: [
            {
              segment: 'online-news',
              name: 'Person',
              title: 'Person',
              link: '/analyze/top-person/online-news',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/analyze/top-person/online-news/index',
              children: [
                {
                  segment: 'index',
                  name: 'News Index Person',
                  title: 'News Index Person',
                  link: '/analyze/top-person/online-news/index',
                  icon: 'icon-file-text2',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'issue',
                  name: 'News Issue Person',
                  title: 'News Issue Person',
                  link: '/analyze/top-person/online-news/issue',
                  icon: 'icon-copy',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'map',
                  name: 'News Map Person',
                  title: 'News Map Person',
                  link: '/analyze/top-person/online-news/map',
                  icon: 'icon-map5',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'printed-news',
              name: 'Person',
              title: 'Person',
              link: '/analyze/top-person/printed-news',
              icon: 'icon-printer4',
              main_page: false,
              display: true,
              redirect_to: '/analyze/top-person/printed-news/index',
              children: [
                {
                  segment: 'index',
                  name: 'Printed News Index Person',
                  title: 'Printed News Index Person',
                  link: '/analyze/top-person/printed-news/index',
                  icon: 'icon-file-text2',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
          ]
        },
        {
          segment: 'influencer',
          name: 'Influencer',
          title: 'Influencer',
          link: '/analyze/influencer',
          icon: 'sticky note outline icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/influencer/online-news/index',
          children: [
            {
              segment: 'online-news',
              name: 'Influencer',
              title: 'Influencer',
              link: '/analyze/influencer/online-news',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/analyze/influencer/online-news/index',
              children: [
                {
                  segment: 'index',
                  name: 'News Index Influencer',
                  title: 'News Index Influencer',
                  link: '/analyze/influencer/online-news/index',
                  icon: 'icon-file-text2',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'sentiment',
                  name: 'News Sentiment Influencer',
                  title: 'News Sentiment Influencer',
                  link: '/analyze/influencer/online-news/sentiment',
                  icon: 'icon-heart6',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'issue',
                  name: 'News Issue Influencer',
                  title: 'News Issue Influencer',
                  link: '/analyze/influencer/online-news/issue',
                  icon: 'icon-copy',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'map',
                  name: 'News Map Influencer',
                  title: 'News Map Influencer',
                  link: '/analyze/influencer/online-news/map',
                  icon: 'icon-map5',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'twitter',
              name: 'Influencer',
              title: 'Influencer',
              link: '/analyze/influencer/twitter',
              icon: 'icon-twitter',
              main_page: false,
              display: true,
              redirect_to: '/analyze/influencer/twitter/index',
              children: [
                {
                  segment: 'index',
                  name: 'Twitter Index Influencer',
                  title: 'Twitter Index Influencer',
                  link: '/analyze/influencer/twitter/index',
                  icon: 'icon-file-text2',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'sentiment',
                  name: 'Twitter Sentiment Influencer',
                  title: 'Twitter Sentiment Influencer',
                  link: '/analyze/influencer/twitter/sentiment',
                  icon: 'icon-heart6',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'facebook',
              name: 'Influencer',
              title: 'Influencer',
              link: '/analyze/influencer/facebook',
              icon: 'icon-facebook',
              main_page: false,
              display: true,
              redirect_to: '/analyze/influencer/facebook/index',
              children: [
                {
                  segment: 'index',
                  name: 'Facebook Index Influencer',
                  title: 'Facebook Index Influencer',
                  link: '/analyze/influencer/facebook/index',
                  icon: 'icon-file-text2',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'sentiment',
                  name: 'Facebook Sentiment Influencer',
                  title: 'Facebook Sentiment Influencer',
                  link: '/analyze/influencer/facebook/sentiment',
                  icon: 'icon-heart6',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'printed-news',
              name: 'Influencer',
              title: 'Influencer',
              link: '/analyze/influencer/printed-news',
              icon: 'icon-printer4',
              main_page: false,
              display: true,
              redirect_to: '/analyze/influencer/printed-news/index',
              children: [
                {
                  segment: 'index',
                  name: 'Printed News Index Influencer',
                  title: 'Printed News Index Influencer',
                  link: '/analyze/influencer/printed-news/index',
                  icon: 'icon-file-text2',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'sentiment',
                  name: 'Printed News Sentiment Influencer',
                  title: 'Printed News Sentiment Influencer',
                  link: '/analyze/influencer/printed-news/sentiment',
                  icon: 'icon-heart6',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
          ]
        },
        {
          segment: 'media-share',
          name: 'Media Share',
          title: 'Media Share',
          link: '/analyze/media-share',
          icon: 'chart pie icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/media-share/online-news/index',
          children: [
            {
              segment: 'online-news',
              name: 'Media Share',
              title: 'Media Share',
              link: '/analyze/media-share/online-news',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/analyze/media-share/online-news/index',
              children: [
                {
                  segment: 'index',
                  name: 'News Index Media Share',
                  title: 'News Index Media Share',
                  link: '/analyze/media-share/online-news/index',
                  icon: 'icon-file-text2',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'sentiment',
                  name: 'News Sentiment Media Share',
                  title: 'News Sentiment Media Share',
                  link: '/analyze/media-share/online-news/sentiment',
                  icon: 'icon-heart6',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'compare',
                  name: 'News Compare Media Share',
                  title: 'News Compare Media Share',
                  link: '/analyze/media-share/online-news/compare',
                  icon: 'icon-enlarge3',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'printed-news',
              name: 'Media Share',
              title: 'Media Share',
              link: '/analyze/media-share/printed-news',
              icon: 'icon-printer4',
              main_page: false,
              display: true,
              redirect_to: '/analyze/media-share/printed-news/index',
              children: [
                {
                  segment: 'index',
                  name: 'Printed News Index Media Share',
                  title: 'Printed News Index Media Share',
                  link: '/analyze/media-share/printed-news/index',
                  icon: 'icon-file-text2',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'sentiment',
                  name: 'Printed News Sentiment Media Share',
                  title: 'Printed News Sentiment Media Share',
                  link: '/analyze/media-share/printed-news/sentiment',
                  icon: 'icon-heart6',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'compare',
                  name: 'Printed News Compare Media Share',
                  title: 'Printed News Compare Media Share',
                  link: '/analyze/media-share/printed-news/compare',
                  icon: 'icon-enlarge3',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'tv-news',
              name: 'Media Share',
              title: 'Media Share',
              link: '/analyze/media-share/tv-news',
              icon: 'icon-tv',
              main_page: false,
              display: true,
              redirect_to: '/analyze/media-share/tv-news/index',
              children: [
                {
                  segment: 'index',
                  name: 'TV News Index Media Share',
                  title: 'TV News Index Media Share',
                  link: '/analyze/media-share/tv-news/index',
                  icon: 'icon-file-text2',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'sentiment',
                  name: 'TV News Sentiment Media Share',
                  title: 'TV News Sentiment Media Share',
                  link: '/analyze/media-share/tv-news/sentiment',
                  icon: 'icon-heart6',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'compare',
                  name: 'TV News Compare Media Share',
                  title: 'TV News Compare Media Share',
                  link: '/analyze/media-share/tv-news/compare',
                  icon: 'icon-enlarge3',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
          ]
        },
        {
          segment: 'sentiment',
          name: 'Sentiment',
          title: 'Sentiment',
          link: '/analyze/sentiment',
          icon: 'heart outline icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/sentiment/online-news/content',
          children: [
            {
              segment: 'online-news',
              name: 'Sentiment',
              title: 'Sentiment',
              link: '/analyze/sentiment/online-news',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/analyze/sentiment/online-news/content',
              children: [
                {
                  segment: 'content',
                  name: 'News Content Sentiment',
                  title: 'News Content Sentiment',
                  link: '/analyze/sentiment/online-news/content',
                  icon: 'icon-files-empty',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'influencer',
                  name: 'News Influencer Sentiment',
                  title: 'News Influencer Sentiment',
                  link: '/analyze/sentiment/online-news/influencer',
                  icon: 'icon-bubbles8',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'twitter',
              name: 'Sentiment',
              title: 'Sentiment',
              link: '/analyze/sentiment/twitter',
              icon: 'icon-twitter',
              main_page: false,
              display: true,
              redirect_to: '/analyze/sentiment/twitter/content',
              children: [
                {
                  segment: 'content',
                  name: 'Twitter Content Sentiment',
                  title: 'Twitter Content Sentiment',
                  link: '/analyze/sentiment/twitter/content',
                  icon: 'icon-files-empty',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'influencer',
                  name: 'Twitter Influencer Sentiment',
                  title: 'Twitter Influencer Sentiment',
                  link: '/analyze/sentiment/twitter/influencer',
                  icon: 'icon-bubbles8',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'facebook',
              name: 'Sentiment',
              title: 'Sentiment',
              link: '/analyze/sentiment/facebook',
              icon: 'icon-facebook',
              main_page: false,
              display: true,
              redirect_to: '/analyze/sentiment/facebook/content',
              children: [
                {
                  segment: 'content',
                  name: 'Facebook Content Sentiment',
                  title: 'Facebook Content Sentiment',
                  link: '/analyze/sentiment/facebook/content',
                  icon: 'icon-files-empty',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'influencer',
                  name: 'Facebook Influencer Sentiment',
                  title: 'Facebook Influencer Sentiment',
                  link: '/analyze/sentiment/facebook/influencer',
                  icon: 'icon-bubbles8',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'printed-news',
              name: 'Sentiment',
              title: 'Sentiment',
              link: '/analyze/sentiment/printed-news',
              icon: 'icon-printer4',
              main_page: false,
              display: true,
              redirect_to: '/analyze/sentiment/printed-news/content',
              children: [
                {
                  segment: 'content',
                  name: 'Printed News Content Sentiment',
                  title: 'Printed News Content Sentiment',
                  link: '/analyze/sentiment/printed-news/content',
                  icon: 'icon-files-empty',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'influencer',
                  name: 'Printed News Influencer Sentiment',
                  title: 'Printed News Influencer Sentiment',
                  link: '/analyze/sentiment/printed-news/influencer',
                  icon: 'icon-bubbles8',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
          ]
        },
        {
          segment: 'map',
          name: 'Map',
          title: 'Map',
          link: '/analyze/map',
          icon: 'map outline icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/map/exposure/news',
          children: [
            {
              segment: 'exposure',
              name: 'Map',
              title: 'Map',
              link: '/analyze/map/exposure',
              icon: 'icon-statistics',
              main_page: false,
              display: true,
              redirect_to: '/analyze/map/exposure/news',
              children: [
                {
                  segment: 'news',
                  name: 'Map News',
                  title: 'Map News',
                  link: '/analyze/map/exposure/news',
                  icon: 'icon-sphere',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'twitter',
                  name: 'Map News Twitter',
                  title: 'Map News Twitter',
                  link: '/analyze/map/exposure/twitter',
                  icon: 'icon-twitter',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'issue',
              name: 'Map Issue',
              title: 'Map Issue',
              link: '/analyze/map/issue',
              icon: 'icon-fire',
              main_page: false,
              display: true,
              redirect_to: '/analyze/map/issue',
              children: []
            },
            {
              segment: 'sentiment',
              name: 'Map Sentiment',
              title: 'Map Sentiment',
              link: '/analyze/map/sentiment',
              icon: 'icon-heart6',
              main_page: false,
              display: true,
              redirect_to: '/analyze/map/sentiment',
              children: []
            },
          ]
        },
        {
          segment: 'ontology',
          name: 'Ontology',
          title: 'Ontology',
          link: '/analyze/ontology',
          icon: 'object group outline icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/ontology',
          children: []
        },
        {
          segment: 'graph-news',
          name: 'Graph News',
          title: 'Graph News',
          link: '/analyze/graph-news',
          icon: 'hubspot icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/graph-news/person',
          children: [
            {
              segment: 'person',
              name: 'Graph News Person',
              title: 'Graph News Person',
              link: '/analyze/graph-news/person',
              icon: 'icon-user',
              main_page: false,
              display: true,
              redirect_to: '/analyze/graph-news/person',
              children: []
            },
            {
              segment: 'influencer',
              name: 'Graph News Influencer',
              title: 'Graph News Influencer',
              link: '/analyze/graph-news/influencer',
              icon: 'icon-bubbles8',
              main_page: false,
              display: true,
              redirect_to: '/analyze/graph-news/influencer',
              children: []
            },
          ]
        },
        {
          segment: 'trending',
          name: 'Trending',
          title: 'Trending',
          link: '/analyze/trending',
          icon: 'chart line icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/trending/online-news/keywords',
          children: [
            {
              segment: 'online-news',
              name: 'Trending',
              title: 'Trending',
              link: '/analyze/trending/online-news/keywords',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/analyze/trending/online-news/keywords',
              children: [
                {
                  segment: 'keywords',
                  name: 'News Keywords Trending',
                  title: 'News Keywords Trending',
                  link: '/analyze/trending/online-news/keywords',
                  icon: 'icon-keyboard',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'people',
                  name: 'News People Trending',
                  title: 'News People Trending',
                  link: '/analyze/trending/online-news/people',
                  icon: 'icon-users',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'phrase',
                  name: 'News Phrase Trending',
                  title: 'News Phrase Trending',
                  link: '/analyze/trending/online-news/phrase',
                  icon: 'icon-shrink6',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'printed-news',
              name: 'Trending',
              title: 'Trending',
              link: '/analyze/trending/printed-news/keywords',
              icon: 'icon-printer4',
              main_page: false,
              display: true,
              redirect_to: '/analyze/trending/printed-news/keywords',
              children: [
                {
                  segment: 'keywords',
                  name: 'Printed News Keywords Trending',
                  title: 'Printed News Keywords Trending',
                  link: '/analyze/trending/printed-news/keywords',
                  icon: 'icon-keyboard',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'people',
                  name: 'Printed News People Trending',
                  title: 'Printed News People Trending',
                  link: '/analyze/trending/printed-news/people',
                  icon: 'icon-users',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'phrase',
                  name: 'Printed News Phrase Trending',
                  title: 'Printed News Phrase Trending',
                  link: '/analyze/trending/printed-news/phrase',
                  icon: 'icon-shrink6',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
          ]
        },
        {
          segment: 'report',
          name: 'Report',
          title: 'Report',
          link: '/analyze/report',
          icon: 'file pdf outline icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/report/online-news/reporting',
          children: [
            {
              segment: 'online-news',
              name: 'Report',
              title: 'Report',
              link: '/analyze/report/online-news',
              icon: 'icon-list3',
              main_page: false,
              display: true,
              redirect_to: '/analyze/report/online-news/reporting',
              children: [
                {
                  segment: 'reporting',
                  name: 'News Report',
                  title: 'News Report',
                  link: '/analyze/report/online-news/reporting',
                  icon: 'icon-calculator3',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'daily',
                  name: 'News Daily Report',
                  title: 'News Daily Report',
                  link: '/analyze/report/online-news/daily',
                  icon: 'icon-history',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'printed-news',
              name: 'Report',
              title: 'Report',
              link: '/analyze/report/printed-news',
              icon: 'icon-printer4',
              main_page: false,
              display: true,
              redirect_to: '/analyze/report/printed-news/reporting',
              children: [
                {
                  segment: 'reporting',
                  name: 'Printed News Report',
                  title: 'Printed News Report',
                  link: '/analyze/report/printed-news/reporting',
                  icon: 'icon-calculator3',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'daily',
                  name: 'Printed News Daily Report',
                  title: 'Printed News Daily Report',
                  link: '/analyze/report/printed-news/daily',
                  icon: 'icon-history',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
          ]
        },
        {
          segment: 'trending-issue',
          name: 'Trending Issue',
          title: 'Trending Issue',
          link: '/analyze/trending-issue',
          icon: 'icon star',
          main_page: false,
          display: true,
          redirect_to: '/analyze/trending-issue',
          children: []
        },
        {
          segment: 'circle-analyze',
          name: 'Circle Analyze',
          title: 'Circle Analyze',
          link: '/analyze/circle-analyze',
          icon: 'circle outline icon',
          main_page: false,
          display: true,
          redirect_to: '/analyze/circle-analyze',
          children: []
        },
      ]
    },
    {
      segment: 'action',
      name: 'Action',
      title: 'Action',
      link: '/analyze',
      icon: 'icon-dots',
      main_page: false,
      display: true,
      redirect_to: '/action',
      children: [
        {
          segment: 'dependency-criteria',
          name: 'Dependency Criteria',
          title: 'Dependency Criteria',
          link: '/action/dependency-criteria',
          icon: 'cubes icon',
          main_page: false,
          display: true,
          redirect_to: '/action/dependency-criteria/index',
          children: [
            {
              segment: 'index',
              name: 'Index Dependency Criteria',
              title: 'Index Dependency Criteria',
              link: '/action/dependency-criteria/index',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/action/dependency-criteria/index',
              children: []
            },
            {
              segment: 'vol',
              name: 'Vol',
              title: 'Vol',
              link: '/action/dependency-criteria/vol',
              icon: 'icon-bars-alt',
              main_page: false,
              display: true,
              redirect_to: '/action/dependency-criteria/vol/news',
              children: [
                {
                  segment: 'news',
                  name: 'Vol News',
                  title: 'Vol News',
                  link: '/action/dependency-criteria/vol/news',
                  icon: 'icon-sphere',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'statements',
                  name: 'Vol Statements',
                  title: 'Vol Statements',
                  link: '/action/dependency-criteria/vol/statements',
                  icon: 'icon-bubble-lines4',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'opinions',
                  name: 'Vol Opinions',
                  title: 'Vol Opinions',
                  link: '/action/dependency-criteria/vol/opinions',
                  icon: 'icon-bubbles5',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'influencer',
              name: 'Influencer',
              title: 'Influencer',
              link: '/action/dependency-criteria/influencer',
              icon: 'icon-users2',
              main_page: false,
              display: true,
              redirect_to: '/action/dependency-criteria/influencer/statements',
              children: [
                {
                  segment: 'statements',
                  name: 'Influencer Statements',
                  title: 'Influencer Statements',
                  link: '/action/dependency-criteria/influencer/statements',
                  icon: 'icon-bubble-lines4',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'opinions',
                  name: 'Influencer Opinions',
                  title: 'Influencer Opinions',
                  link: '/action/dependency-criteria/influencer/opinions',
                  icon: 'icon-bubbles5',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'media',
              name: 'Media Dependency Criteria',
              title: 'Media Dependency Criteria',
              link: '/action/dependency-criteria/media',
              icon: 'icon-newspaper2',
              main_page: false,
              display: true,
              redirect_to: '/action/dependency-criteria/media',
              children: []
            },
            {
              segment: 'sentiment',
              name: 'Sentiment',
              title: 'Sentiment',
              link: '/action/dependency-criteria/sentiment',
              icon: 'icon-heart6',
              main_page: false,
              display: true,
              redirect_to: '/action/dependency-criteria/sentiment/news',
              children: [
                {
                  segment: 'news',
                  name: 'Sentiment News',
                  title: 'Sentiment News',
                  link: '/action/dependency-criteria/sentiment/news',
                  icon: 'icon-sphere',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'statements',
                  name: 'Sentiment Statements',
                  title: 'Sentiment Statements',
                  link: '/action/dependency-criteria/sentiment/statements',
                  icon: 'icon-bubble-lines4',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'opinions',
                  name: 'Sentiment Opinions',
                  title: 'Sentiment Opinions',
                  link: '/action/dependency-criteria/sentiment/opinions',
                  icon: 'icon-bubbles5',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'ontology',
              name: 'Ontology',
              title: 'Ontology',
              link: '/action/dependency-criteria/ontology',
              icon: 'icon-tree5',
              main_page: false,
              display: true,
              redirect_to: '/action/dependency-criteria/ontology/influencer',
              children: [
                {
                  segment: 'influencer',
                  name: 'Ontology Influencer',
                  title: 'Ontology Influencer',
                  link: '/action/dependency-criteria/ontology/influencer',
                  icon: 'icon-users2',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'top-person',
                  name: 'Ontology Top Person',
                  title: 'Ontology Top Person',
                  link: '/action/dependency-criteria/ontology/top-person',
                  icon: 'icon-user-check',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'news',
                  name: 'Ontology News',
                  title: 'Ontology News',
                  link: '/action/dependency-criteria/ontology/news',
                  icon: 'icon-sphere',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
          ]
        },
        {
          segment: 'dependency-twitter',
          name: 'Dependency Twitter',
          title: 'Dependency Twitter',
          link: '/action/dependency-twitter',
          icon: 'twitter icon',
          main_page: false,
          display: true,
          redirect_to: '/action/dependency-twitter/index',
          children: [
            {
              segment: 'index',
              name: 'Index Dependency Twitter',
              title: 'Index Dependency Twitter',
              link: '/action/dependency-twitter/index',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/action/dependency-twitter/index',
              children: []
            },
            {
              segment: 'vol',
              name: 'Vol',
              title: 'Vol',
              link: '/action/dependency-twitter/vol',
              icon: 'icon-bars-alt',
              main_page: false,
              display: true,
              redirect_to: '/action/dependency-twitter/vol/tweets',
              children: [
                {
                  segment: 'tweets',
                  name: 'Vol Tweets',
                  title: 'Vol Tweets',
                  link: '/action/dependency-twitter/vol/tweets',
                  icon: 'icon-tumblr2',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'rt-mentions',
                  name: 'Vol RT & Mentions',
                  title: 'Vol RT & Mentions',
                  link: '/action/dependency-twitter/vol/rt-mentions',
                  icon: 'icon-bell3',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'follower',
              name: 'Follower',
              title: 'Follower',
              link: '/action/dependency-twitter/follower',
              icon: 'icon-users2',
              main_page: false,
              display: true,
              redirect_to: '/action/dependency-twitter/follower/total',
              children: [
                {
                  segment: 'total',
                  name: 'Follower Total',
                  title: 'Follower Total',
                  link: '/action/dependency-twitter/follower/total',
                  icon: 'icon-sigma',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'new',
                  name: 'Follower New',
                  title: 'Follower New',
                  link: '/action/dependency-twitter/follower/new',
                  icon: 'icon-sphere',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'old',
                  name: 'Follower Old',
                  title: 'Follower Old',
                  link: '/action/dependency-twitter/follower/old',
                  icon: 'icon-accessibility',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'activity',
              name: 'Activity Dependency Twitter',
              title: 'Activity Dependency Twitter',
              link: '/action/dependency-twitter/activity',
              icon: 'icon-pie-chart3',
              main_page: false,
              display: true,
              redirect_to: '/action/dependency-twitter/activity',
              children: []
            },
          ]
        },
        {
          segment: 'dependency-facebook',
          name: 'Dependency Facebook',
          title: 'Dependency Facebook',
          link: '/action/dependency-facebook',
          icon: 'facebook f icon',
          main_page: false,
          display: true,
          redirect_to: '/action/dependency-facebook/index',
          children: [
            {
              segment: 'index',
              name: 'Index Dependency Facebook',
              title: 'Index Dependency Facebook',
              link: '/action/dependency-facebook/index',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/action/dependency-facebook/index',
              children: []
            },
            {
              segment: 'vol',
              name: 'Vol',
              title: 'Vol',
              link: '/action/dependency-facebook/vol',
              icon: 'icon-bars-alt',
              main_page: false,
              display: true,
              redirect_to: '/action/dependency-facebook/vol/posts',
              children: [
                {
                  segment: 'posts',
                  name: 'Vol Posts',
                  title: 'Vol Posts',
                  link: '/action/dependency-facebook/vol/posts',
                  icon: 'icon-mail5',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
                {
                  segment: 'comments',
                  name: 'Vol Comments',
                  title: 'Vol Comments',
                  link: '/action/dependency-facebook/vol/comments',
                  icon: 'icon-file-text2',
                  main_page: false,
                  display: true,
                  redirect_to: '',
                  children: []
                },
              ]
            },
            {
              segment: 'activity',
              name: 'Activity Dependency Facebook',
              title: 'Activity Dependency Facebook',
              link: '/action/dependency-facebook/activity',
              icon: 'icon-pie-chart3',
              main_page: false,
              display: true,
              redirect_to: '/action/dependency-facebook/activity',
              children: []
            },
          ]
        },
        {
          segment: 'indicator',
          name: 'Indicator',
          title: 'Indicator',
          link: '/action/indicator',
          icon: 'cloud download icon',
          main_page: false,
          display: true,
          redirect_to: '/action/indicator/index',
          children: [
            {
              segment: 'index',
              name: 'Index Indicator',
              title: 'Index Indicator',
              link: '/action/indicator/index',
              icon: 'icon-circles2',
              main_page: false,
              display: true,
              redirect_to: '/action/indicator/index',
              children: []
            },
            {
              segment: 'exposure',
              name: 'Indicator Exposure',
              title: 'Indicator Exposure',
              link: '/action/indicator/exposure',
              icon: 'icon-statistics',
              main_page: false,
              display: true,
              redirect_to: '/action/indicator/exposure',
              children: []
            },
            {
              segment: 'sentiment',
              name: 'Indicator Sentiment',
              title: 'Indicator Sentiment',
              link: '/action/indicator/sentiment',
              icon: 'icon-bubble-lines4',
              main_page: false,
              display: true,
              redirect_to: '/action/indicator/sentiment',
              children: []
            },
          ]
        },
      ]
    },
    {
      segment: 'settings',
      name: 'Settings',
      title: 'Settings',
      link: '/settings',
      icon: 'user icon mr-0',
      main_page: false,
      display: true,
      redirect_to: '/settings/profile',
      children: [
        {
          segment: 'profile',
          name: 'Profile',
          title: 'Profile',
          link: '/settings/profile',
          icon: 'user outline icon',
          main_page: false,
          display: true,
          redirect_to: '/settings/profile',
          children: []
        },
        {
          segment: 'my-interest',
          name: 'My Interest',
          title: 'My Interest',
          link: '/settings/my-interest',
          icon: 'star half outline icon',
          main_page: false,
          display: true,
          redirect_to: '/settings/my-interest',
          children: []
        },
        {
          segment: 'my-criteria',
          name: 'My Criteria',
          title: 'My Criteria',
          link: '/settings/my-criteria',
          icon: 'file outline icon',
          main_page: false,
          display: true,
          redirect_to: '/settings/my-criteria',
          children: []
        },
        {
          segment: 'criteria',
          name: 'Criteria',
          title: 'Criteria',
          link: '/settings/criteria',
          icon: 'clipboard list icon  ',
          main_page: false,
          display: true,
          redirect_to: '/settings/criteria',
          children: []
        },
        {
          segment: 'administrator',
          name: 'Administrator',
          title: 'Aadministrator',
          link: '/settings/administrator',
          icon: 'cog icon',
          main_page: false,
          display: true,
          redirect_to: '/settings/administrator',
          children: []
        },
      ]
    },
  ];