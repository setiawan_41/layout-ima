import { Component, OnInit } from '@angular/core';
import { APP_MENU } from '../../../../app/config/app-menu.config';
@Component({
  selector: 'app-layout-default',
  templateUrl: './layout-default.component.html',
  styleUrls: ['./layout-default.component.scss']
})
export class LayoutDefaultComponent implements OnInit {
  public appNameLong = 'Pesona  Alam Indonesia';
  public appNameShort = 'PAI';
  public MENU_PAI = APP_MENU;
  public layoutMenu = 'layout-1';
  public rootPathSelected: string;
  constructor() { }

  ngOnInit() {
    // this.activeLayoutMenu();
  }

  // activeLayoutMenu() {
  //   if (this.rootPathSelected === 'discover' || this.rootPathSelected === 'analyze' || this.rootPathSelected === 'action') {
  //     this.layoutMenu = 'layout-2';
  //   }
  // }

}
