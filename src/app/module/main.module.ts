import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainRoutingComponent, MainRouting } from '../routing/main.routing';
// import { MainRouting, MainRoutingComponent } from '../routing/main.routing';
// import { MainRouting, MainRoutingComponent } from '@routes/main.routing';

@NgModule({
    declarations: [
        MainRoutingComponent
    ],
    imports: [
        CommonModule,
        MainRouting,
    ]
})
export class MainModule { }