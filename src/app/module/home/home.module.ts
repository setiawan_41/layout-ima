import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeRoutingComponent, HomeRouting } from 'src/app/routing/home/home.routing';
// import { HomeRouting, HomeRoutingComponent } from 'src/app/routing/home/home.routing';

@NgModule({
  declarations: [
    HomeRoutingComponent
  ],
  imports: [
    CommonModule,
    HomeRouting
  ]
})
export class HomeModule { }